﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DestinyTools
{
    public partial class Form1 : Form
    {
        DateBase db = new DateBase();
        Properties.Settings sett = new Properties.Settings();
        /// <summary>
        /// Логгирование
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="type">0 - Стандарт (по умолчанию), 1 - Запуск/Остановка приложения</param>
        public static void Log(string message, int type = 0)
        {
            switch (type)
            {
                case 0: File.AppendAllText(Application.StartupPath + "\\log.txt", "[" + DateTime.Now.ToShortDateString() + "] " + DateTime.Now.ToLongTimeString() + " - " + message + "\n"); break;
                case 1: File.AppendAllText(Application.StartupPath + "\\log.txt", message + " - " + "[" + DateTime.Now.ToShortDateString() + "] " + DateTime.Now.ToLongTimeString() + "\n"); break;
                //case 2: File.AppendAllText(Application.StartupPath + "\\logNews.txt", "[" + DateTime.Now.ToShortDateString() + "] " + DateTime.Now.ToLongTimeString() + " - " + message + "\n"); break;
            }
        }
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Log("\nЗапуск программы", 1);
            #region version
            string v3 = "", v4 = "";
            string temp = "";
            v4 = "." + ProductVersion.Split('.')[3];
            v3 = "." + ProductVersion.Split('.')[2];
            if (v3 != ".0" && v4 != ".0")
                temp = v3 + v4;
            else if (v3 == ".0" && v4 != ".0")
                temp = ".0" + v4;
            else if (v3 != ".0" && v4 == ".0")
                temp = v4;
           Text += " v. " + ProductVersion.Split('.')[0] + "." + ProductVersion.Split('.')[1] + temp;
            #endregion
            timerMain.Start();
        }

        #region Notify
        private void Form1_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                Hide();
                ShowInTaskbar = false;
                FormBorderStyle = FormBorderStyle.FixedToolWindow;
            }
        }
        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            if (!ShowInTaskbar)
            {
                Show();
                WindowState = FormWindowState.Normal;
                ShowInTaskbar = true;
                FormBorderStyle = FormBorderStyle.Sizable;
                Activate();
            }
            else
            {
                Hide();
                ShowInTaskbar = false;
                FormBorderStyle = FormBorderStyle.FixedToolWindow;
            }
        }
        #endregion

        #region Click_Notify
        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
            ShowInTaskbar = true;
            FormBorderStyle = FormBorderStyle.Sizable;
            Activate();
        }
        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Log("Завершение программы", 1);
            Application.Exit();
        }
        private void найтиВБазеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Search s = new Search();
            s.ShowDialog();
        }
        private void dIMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try { Process.Start(@"https://app.destinyitemmanager.com/"); } catch { }
        }
        private void картаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try {  Process.Start(@"https://lowlidev.com.au/destiny/maps/"); } catch { }
        }
        #endregion

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Log("Завершение программы", 1);
        }

        #region Tables
        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            (contextMenuStrip1.Items["таблицыToolStripMenuItem"] as ToolStripMenuItem).DropDownItems.Clear();
            string path = sett.pathTables;
            if (Directory.Exists(path))
            {
                var dir = new DirectoryInfo(path);
                var files = new List<string>();
                foreach (FileInfo file in dir.GetFiles())
                {
                    files.Add(file.FullName);
                }

                for (int i = 0; i < files.Count; i++)
                {
                    if ((Path.GetExtension(files[i]) == ".xlsx") /*&& (File.GetAttributes(files[i]) == (FileAttributes.Compressed | FileAttributes.Archive) || File.GetAttributes(files[i]) == (FileAttributes.Compressed))*/)
                        (contextMenuStrip1.Items["таблицыToolStripMenuItem"] as ToolStripMenuItem).DropDownItems.Add(Path.GetFileNameWithoutExtension(files[i]).Replace("+", ""));
                }

                foreach (ToolStripMenuItem item in (contextMenuStrip1.Items["таблицыToolStripMenuItem"] as ToolStripMenuItem).DropDownItems)
                    Sub(item, cms_click);
            }

        }
        private void Sub(ToolStripMenuItem item, EventHandler eventHandler)
        {
            item.Click += eventHandler;
        }

        void cms_click(object sender, EventArgs e)
        {
            try { Process.Start(sett.pathTables + "\\" + (sender as ToolStripMenuItem).Text + ".xlsx"); }
            catch (Exception ee) { MessageBox.Show(ee.ToString()); }
        }

        private void xlsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try { Process.Start(sett.pathTables); }
            catch (Exception ee) { MessageBox.Show(ee.ToString()); }
        }


        #endregion

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Settings settings = new Settings();
            settings.ShowDialog();
        }
        #region NightFalls
        /// <summary>
        /// Указать налёты
        /// </summary>
        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            NightFall nf = new NightFall();
            nf.ShowDialog();
        }
        private void updateNightFall()
        {
            var data = db.GetDataOnRequest("SELECT Names, RewardImg, Reward, Info FROM NightFalls WHERE Actual = 1");

            if (data.Rows.Count > 3) MessageBox.Show("Выбрано более 3 активных сумрачных налётов.", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            labelNameNight1.Text = data.Rows[0]["Names"].ToStringNew();
            labelNameNight2.Text = data.Rows[1]["Names"].ToStringNew();
            labelNameNight3.Text = data.Rows[2]["Names"].ToStringNew();

            pictureBoxNight1.ImageLocation = data.Rows[0]["RewardImg"].ToStringNew();
            pictureBoxNight2.ImageLocation = data.Rows[1]["RewardImg"].ToStringNew();
            pictureBoxNight3.ImageLocation = data.Rows[2]["RewardImg"].ToStringNew();

            pictureBoxNight1.Load();
            pictureBoxNight2.Load();
            pictureBoxNight3.Load();

            labelReward1.Text = data.Rows[0]["Reward"].ToStringNew();
            labelReward2.Text = data.Rows[1]["Reward"].ToStringNew();
            labelReward3.Text = data.Rows[2]["Reward"].ToStringNew();
        }

        bool busy = false;
        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (busy) return;
            busy = true;
            if (linkLabel5.Text == "Развернуть")
            {
                while (Height < 471)
                {
                    Height++;
                    Application.DoEvents();
                }
                linkLabel5.Text = "Свернуть";
            }
            else
            {
                while (Height > 414)
                {
                    Height--;
                    Application.DoEvents();
                }
                linkLabel5.Text = "Развернуть";
            }
            busy = false;
        }

        #endregion

        private void WhereIsXur()
        {
            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Friday:

                    break;
                case DayOfWeek.Saturday:
                case DayOfWeek.Sunday:
                case DayOfWeek.Monday:
                    break;
                case DayOfWeek.Tuesday:
                    break;
                default:
                    break;
            }
            if (DateTime.Now.DayOfWeek >= DayOfWeek.Friday && DateTime.Now.DayOfWeek <= DayOfWeek.Tuesday)
            {
               // if (date)
            }
        }
        /// <summary>
        /// Главный таймер.
        /// </summary>
        private void timerMain_Tick(object sender, EventArgs e)
        {
            #region Timers Reset
            //Недельный
            int start = (int)DateTime.Now.DayOfWeek;
            int target = (int)DayOfWeek.Tuesday;
            if (target <= start)
                target += 7;
            var r = DateTime.Now.AddDays(target - start);

            TimeSpan next = new DateTime(r.Year, r.Month, r.Day, 20, 0, 0) - DateTime.Now;
            labelTimerWeek.Text = ((next.Days != 0) ? next.Days + " д. ": "") + next.Hours + " ч. " + next.Minutes + " м.";

            //Дневной
            DateTime d1;
            if (DateTime.Now.Hour > 20)
                d1 = DateTime.Now.AddDays(1);
            else
                d1 = DateTime.Now;

            TimeSpan nextDay = new DateTime(d1.Year, d1.Month, d1.Day, 20, 0, 0) - DateTime.Now;
            labelTimerDay.Text = ((nextDay.Days != 0) ? nextDay.Days + " д. " : "") + nextDay.Hours + " ч. " + nextDay.Minutes + " м.";

            //Зур
            WhereIsXur();
            #endregion
        }
    }
    /// <summary>
    /// Добавление необходимых методов для любых типов.
    /// </summary>
    static class Expansion
    {
        /// <summary>
        /// Пытается привести объект к int32, если это невозможно, вернёт 0.
        /// </summary>
        /// <param name="ob">Объект</param>
        /// <returns></returns>
        public static int ToInt(this object ob)
        {
            int res = 0;
            try { res = Convert.ToInt32(ob); }
            catch { }
            return res;
        }
        /// <summary>
        /// Пытается привести объект к int32, если это невозможно, вернёт -1.
        /// </summary>
        /// <param name="ob">Объект</param>
        /// <returns></returns>
        public static int ToInt(this object ob, bool GetMeMinus)
        {
            int res = -1;
            try { res = Convert.ToInt32(ob); }
            catch { }
            return res;
        }
        /// <summary>
        /// Пытается привести объект к string, если это невозможно, вернёт пустую строку.
        /// </summary>
        /// <param name="ob"></param>
        /// <returns></returns>
        public static string ToStringNew(this object ob)
        {
            string value = "";
            try { value = Convert.ToString(ob); }
            catch { }
            return value;
        }
        /// <summary>
        /// Проверяет, необходимо ли использовать метод Invoke, и использует его, если нужно.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="action">Action action = () => {Код};</param>
        public static void InvokeCheck(this Control control, Action action)
        {
            try
            {
                if (control.InvokeRequired)
                    control.Invoke(action);
                else
                    action();
            }
            catch
            { }
        }
    }
    enum typeNightFall
    {
        Первый,
        Второй,
        Третий
    }
}
