﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DestinyTools
{
    class DateBase
    {
        #region Работа с БД
        public OleDbConnection Connect()
        {
            string nowDate = DateTime.Now.ToShortDateString().Replace(".", "_");
            string ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + Application.StartupPath + "\\db.mdb";
            OleDbConnection conn = new OleDbConnection();
            conn.ConnectionString = ConnectionString;
            return conn;
        }
        public DataTable GetDataOnRequest(string SQLrequest)
        {
            var c = Connect();
            c.Open();
            DataTable data = new DataTable();
            OleDbCommand SQLQuery = new OleDbCommand(SQLrequest, c);
            OleDbDataAdapter dataAdapter = new OleDbDataAdapter(SQLQuery);
            try
            {
                dataAdapter.Fill(data);
            }
            catch (Exception q) { MessageBox.Show("Ошибка: " + q.Message + "\r\rЗапрос: " + SQLrequest, "Ошибка запроса.", MessageBoxButtons.OK, MessageBoxIcon.Warning); }
            c.Dispose();
            return data;
        }
        public int SetDataOnRequest(string SQLrequest)
        {
            var c = Connect();
            c.Open();
            int count = 0;
            OleDbCommand SQLQuery = new OleDbCommand(SQLrequest, c);
            try
            {
                count = SQLQuery.ExecuteNonQuery();
            }
            catch (Exception q) { MessageBox.Show("Ошибка: " + q.Message + "\r\rЗапрос: " + SQLrequest, "Ошибка запроса.", MessageBoxButtons.OK, MessageBoxIcon.Warning); }

            c.Dispose();
            return count;
        }
        #endregion
    }
}
