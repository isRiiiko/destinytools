﻿namespace DestinyTools
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.открытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.таблицыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.найтиВБазеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dIMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.картаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelTimerDay = new System.Windows.Forms.Label();
            this.labelTimerWeek = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelReward3 = new System.Windows.Forms.Label();
            this.labelReward2 = new System.Windows.Forms.Label();
            this.labelReward1 = new System.Windows.Forms.Label();
            this.pictureBoxNight3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxNight2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxNight1 = new System.Windows.Forms.PictureBox();
            this.labelNameNight3 = new System.Windows.Forms.Label();
            this.labelNameNight2 = new System.Windows.Forms.Label();
            this.labelNameNight1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.linkLabel4 = new System.Windows.Forms.LinkLabel();
            this.linkLabel5 = new System.Windows.Forms.LinkLabel();
            this.textBoxInfo1 = new System.Windows.Forms.TextBox();
            this.textBoxInfo2 = new System.Windows.Forms.TextBox();
            this.textBoxInfo3 = new System.Windows.Forms.TextBox();
            this.timerMain = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNight3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNight2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNight1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.открытьToolStripMenuItem,
            this.таблицыToolStripMenuItem,
            this.toolStripSeparator2,
            this.найтиВБазеToolStripMenuItem,
            this.dIMToolStripMenuItem,
            this.картаToolStripMenuItem,
            this.toolStripSeparator1,
            this.выходToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(145, 148);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // открытьToolStripMenuItem
            // 
            this.открытьToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.открытьToolStripMenuItem.Name = "открытьToolStripMenuItem";
            this.открытьToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.открытьToolStripMenuItem.Text = "Открыть";
            this.открытьToolStripMenuItem.Click += new System.EventHandler(this.открытьToolStripMenuItem_Click);
            // 
            // таблицыToolStripMenuItem
            // 
            this.таблицыToolStripMenuItem.Name = "таблицыToolStripMenuItem";
            this.таблицыToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.таблицыToolStripMenuItem.Text = "Таблицы";
            this.таблицыToolStripMenuItem.Click += new System.EventHandler(this.xlsToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(141, 6);
            // 
            // найтиВБазеToolStripMenuItem
            // 
            this.найтиВБазеToolStripMenuItem.Name = "найтиВБазеToolStripMenuItem";
            this.найтиВБазеToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.найтиВБазеToolStripMenuItem.Text = "Найти в базе";
            this.найтиВБазеToolStripMenuItem.Click += new System.EventHandler(this.найтиВБазеToolStripMenuItem_Click);
            // 
            // dIMToolStripMenuItem
            // 
            this.dIMToolStripMenuItem.Name = "dIMToolStripMenuItem";
            this.dIMToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.dIMToolStripMenuItem.Text = "DIM";
            this.dIMToolStripMenuItem.Click += new System.EventHandler(this.dIMToolStripMenuItem_Click);
            // 
            // картаToolStripMenuItem
            // 
            this.картаToolStripMenuItem.Name = "картаToolStripMenuItem";
            this.картаToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.картаToolStripMenuItem.Text = "Карта";
            this.картаToolStripMenuItem.Click += new System.EventHandler(this.картаToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(141, 6);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // labelTimerDay
            // 
            this.labelTimerDay.Location = new System.Drawing.Point(229, 9);
            this.labelTimerDay.Name = "labelTimerDay";
            this.labelTimerDay.Size = new System.Drawing.Size(102, 23);
            this.labelTimerDay.TabIndex = 1;
            this.labelTimerDay.Text = "21 ч. 25 м.";
            this.labelTimerDay.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTimerWeek
            // 
            this.labelTimerWeek.Location = new System.Drawing.Point(229, 32);
            this.labelTimerWeek.Name = "labelTimerWeek";
            this.labelTimerWeek.Size = new System.Drawing.Size(102, 23);
            this.labelTimerWeek.TabIndex = 1;
            this.labelTimerWeek.Text = "21 ч. 25 м.";
            this.labelTimerWeek.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Горячая точка: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(95, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "ЕМЗ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Зур: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(44, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "3 д. 22 ч.";
            // 
            // label8
            // 
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(6, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(130, 19);
            this.label8.TabIndex = 3;
            this.label8.Text = "Залив какогото света";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(266, 58);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(65, 13);
            this.linkLabel1.TabIndex = 4;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Статистика";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.textBoxInfo3);
            this.groupBox1.Controls.Add(this.textBoxInfo2);
            this.groupBox1.Controls.Add(this.textBoxInfo1);
            this.groupBox1.Controls.Add(this.linkLabel5);
            this.groupBox1.Controls.Add(this.labelReward3);
            this.groupBox1.Controls.Add(this.labelReward2);
            this.groupBox1.Controls.Add(this.labelReward1);
            this.groupBox1.Controls.Add(this.pictureBoxNight3);
            this.groupBox1.Controls.Add(this.pictureBoxNight2);
            this.groupBox1.Controls.Add(this.pictureBoxNight1);
            this.groupBox1.Controls.Add(this.labelNameNight3);
            this.groupBox1.Controls.Add(this.labelNameNight2);
            this.groupBox1.Controls.Add(this.labelNameNight1);
            this.groupBox1.Location = new System.Drawing.Point(12, 218);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(319, 200);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Сумрачные налёты";
            // 
            // labelReward3
            // 
            this.labelReward3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelReward3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelReward3.Location = new System.Drawing.Point(217, 105);
            this.labelReward3.Name = "labelReward3";
            this.labelReward3.Size = new System.Drawing.Size(93, 31);
            this.labelReward3.TabIndex = 2;
            this.labelReward3.Text = "Награда 3";
            this.labelReward3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelReward2
            // 
            this.labelReward2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelReward2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelReward2.Location = new System.Drawing.Point(112, 105);
            this.labelReward2.Name = "labelReward2";
            this.labelReward2.Size = new System.Drawing.Size(93, 31);
            this.labelReward2.TabIndex = 2;
            this.labelReward2.Text = "Награда 2";
            this.labelReward2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelReward1
            // 
            this.labelReward1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelReward1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelReward1.Location = new System.Drawing.Point(7, 105);
            this.labelReward1.Name = "labelReward1";
            this.labelReward1.Size = new System.Drawing.Size(93, 31);
            this.labelReward1.TabIndex = 2;
            this.labelReward1.Text = "Награда 1";
            this.labelReward1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBoxNight3
            // 
            this.pictureBoxNight3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxNight3.Location = new System.Drawing.Point(217, 41);
            this.pictureBoxNight3.Name = "pictureBoxNight3";
            this.pictureBoxNight3.Size = new System.Drawing.Size(93, 61);
            this.pictureBoxNight3.TabIndex = 1;
            this.pictureBoxNight3.TabStop = false;
            // 
            // pictureBoxNight2
            // 
            this.pictureBoxNight2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxNight2.Location = new System.Drawing.Point(112, 41);
            this.pictureBoxNight2.Name = "pictureBoxNight2";
            this.pictureBoxNight2.Size = new System.Drawing.Size(93, 61);
            this.pictureBoxNight2.TabIndex = 1;
            this.pictureBoxNight2.TabStop = false;
            // 
            // pictureBoxNight1
            // 
            this.pictureBoxNight1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxNight1.Location = new System.Drawing.Point(7, 41);
            this.pictureBoxNight1.Name = "pictureBoxNight1";
            this.pictureBoxNight1.Size = new System.Drawing.Size(93, 61);
            this.pictureBoxNight1.TabIndex = 1;
            this.pictureBoxNight1.TabStop = false;
            // 
            // labelNameNight3
            // 
            this.labelNameNight3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelNameNight3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelNameNight3.Location = new System.Drawing.Point(217, 20);
            this.labelNameNight3.Name = "labelNameNight3";
            this.labelNameNight3.Size = new System.Drawing.Size(93, 17);
            this.labelNameNight3.TabIndex = 0;
            this.labelNameNight3.Text = "Налёт 3";
            this.labelNameNight3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelNameNight2
            // 
            this.labelNameNight2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelNameNight2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelNameNight2.Location = new System.Drawing.Point(112, 20);
            this.labelNameNight2.Name = "labelNameNight2";
            this.labelNameNight2.Size = new System.Drawing.Size(93, 17);
            this.labelNameNight2.TabIndex = 0;
            this.labelNameNight2.Text = "Налёт 2";
            this.labelNameNight2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelNameNight1
            // 
            this.labelNameNight1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelNameNight1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelNameNight1.Location = new System.Drawing.Point(7, 20);
            this.labelNameNight1.Name = "labelNameNight1";
            this.labelNameNight1.Size = new System.Drawing.Size(93, 17);
            this.labelNameNight1.TabIndex = 0;
            this.labelNameNight1.Text = "Налёт 1";
            this.labelNameNight1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.pictureBox4);
            this.groupBox2.Location = new System.Drawing.Point(12, 84);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(142, 128);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Высшее испытание";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox4.Location = new System.Drawing.Point(6, 37);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(130, 85);
            this.pictureBox4.TabIndex = 1;
            this.pictureBox4.TabStop = false;
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(266, 87);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(62, 13);
            this.linkLabel2.TabIndex = 4;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Настройка";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // linkLabel3
            // 
            this.linkLabel3.AutoSize = true;
            this.linkLabel3.LinkColor = System.Drawing.Color.Gray;
            this.linkLabel3.Location = new System.Drawing.Point(267, 218);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(50, 13);
            this.linkLabel3.TabIndex = 2;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "Указать";
            this.linkLabel3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel3_LinkClicked);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(178, 149);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 7;
            // 
            // linkLabel4
            // 
            this.linkLabel4.AutoSize = true;
            this.linkLabel4.Location = new System.Drawing.Point(268, 108);
            this.linkLabel4.Name = "linkLabel4";
            this.linkLabel4.Size = new System.Drawing.Size(54, 13);
            this.linkLabel4.TabIndex = 8;
            this.linkLabel4.TabStop = true;
            this.linkLabel4.Text = "Желания";
            // 
            // linkLabel5
            // 
            this.linkLabel5.AutoSize = true;
            this.linkLabel5.LinkColor = System.Drawing.Color.Gray;
            this.linkLabel5.Location = new System.Drawing.Point(119, 0);
            this.linkLabel5.Name = "linkLabel5";
            this.linkLabel5.Size = new System.Drawing.Size(54, 13);
            this.linkLabel5.TabIndex = 3;
            this.linkLabel5.TabStop = true;
            this.linkLabel5.Text = "Свернуть";
            this.linkLabel5.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel5_LinkClicked);
            // 
            // textBoxInfo1
            // 
            this.textBoxInfo1.Location = new System.Drawing.Point(7, 145);
            this.textBoxInfo1.Multiline = true;
            this.textBoxInfo1.Name = "textBoxInfo1";
            this.textBoxInfo1.Size = new System.Drawing.Size(93, 47);
            this.textBoxInfo1.TabIndex = 4;
            this.textBoxInfo1.Text = "Инфо 1\r\nИнфо 2\r\nИнфо 3";
            this.textBoxInfo1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxInfo2
            // 
            this.textBoxInfo2.Location = new System.Drawing.Point(112, 145);
            this.textBoxInfo2.Multiline = true;
            this.textBoxInfo2.Name = "textBoxInfo2";
            this.textBoxInfo2.Size = new System.Drawing.Size(93, 47);
            this.textBoxInfo2.TabIndex = 4;
            this.textBoxInfo2.Text = "Инфо 1\r\nИнфо 2\r\nИнфо 3";
            this.textBoxInfo2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxInfo3
            // 
            this.textBoxInfo3.Location = new System.Drawing.Point(217, 145);
            this.textBoxInfo3.Multiline = true;
            this.textBoxInfo3.Name = "textBoxInfo3";
            this.textBoxInfo3.Size = new System.Drawing.Size(93, 47);
            this.textBoxInfo3.TabIndex = 4;
            this.textBoxInfo3.Text = "Инфо 1\r\nИнфо 2\r\nИнфо 3";
            this.textBoxInfo3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // timerMain
            // 
            this.timerMain.Interval = 1000;
            this.timerMain.Tick += new System.EventHandler(this.timerMain_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 430);
            this.Controls.Add(this.linkLabel4);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.linkLabel3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.linkLabel2);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labelTimerWeek);
            this.Controls.Add(this.labelTimerDay);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DestinyTools";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.contextMenuStrip1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNight3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNight2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNight1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem открытьToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem таблицыToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem найтиВБазеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dIMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem картаToolStripMenuItem;
        private System.Windows.Forms.Label labelTimerDay;
        private System.Windows.Forms.Label labelTimerWeek;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBoxNight3;
        private System.Windows.Forms.PictureBox pictureBoxNight2;
        private System.Windows.Forms.PictureBox pictureBoxNight1;
        private System.Windows.Forms.Label labelNameNight3;
        private System.Windows.Forms.Label labelNameNight2;
        private System.Windows.Forms.Label labelNameNight1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.LinkLabel linkLabel3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.LinkLabel linkLabel4;
        private System.Windows.Forms.Label labelReward3;
        private System.Windows.Forms.Label labelReward2;
        private System.Windows.Forms.Label labelReward1;
        private System.Windows.Forms.LinkLabel linkLabel5;
        private System.Windows.Forms.TextBox textBoxInfo3;
        private System.Windows.Forms.TextBox textBoxInfo2;
        private System.Windows.Forms.TextBox textBoxInfo1;
        private System.Windows.Forms.Timer timerMain;
    }
}

