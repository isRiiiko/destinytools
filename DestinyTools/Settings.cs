﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DestinyTools
{
    public partial class Settings : Form
    {
        Properties.Settings sett = new Properties.Settings();
        public Settings()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            sett.pathClient = textBoxClient.Text;
            sett.pathTables = textBoxTable.Text;
            sett.Save();
            Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void textBoxTable_Click(object sender, EventArgs e)
        {
            TextBox text = (TextBox)sender;
            if (text.Text != "")
                folderBrowserDialog1.SelectedPath = text.Text;

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {

                text.Text = folderBrowserDialog1.SelectedPath;
                text.SelectionStart = text.Text.Length;
            }
        }

        private void textBoxTable_TextChanged(object sender, EventArgs e)
        {
            TextBox text = (TextBox)sender;
            text.SelectionStart = text.Text.Length;
        }
    }
}
