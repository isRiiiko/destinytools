﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DestinyTools
{
    public partial class NightFall : Form
    {
        DateBase db = new DateBase();
        Properties.Settings sett = new Properties.Settings();
        public NightFall()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            db.SetDataOnRequest("UPDATE NightFalls SET Actual = 0");

            foreach (string name in checkedListBox1.CheckedItems)
            {
                if (db.SetDataOnRequest("UPDATE NightFalls SET Actual = 1 WHERE Names = '" + name + "'") == 0)
                    db.SetDataOnRequest("INSERT INTO NightFalls (Names) VALUES ('" + name + "')");
            }
            Close();
        }

        private void NightFall_Load(object sender, EventArgs e)
        {
           var data = db.GetDataOnRequest("SELECT Names, Actual FROM NightFalls");

            for (int i = 0; i < data.Rows.Count; i++)
            {
                checkedListBox1.Items.Add(data.Rows[i][0]);
                if (data.Rows[i][0].ToStringNew() == "1")
                    checkedListBox1.SetItemChecked(checkedListBox1.Items.Count-1, true);
            }
        }
    }
}
