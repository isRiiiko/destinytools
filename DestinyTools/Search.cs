﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DestinyTools
{
    public partial class Search : Form
    {
        string SearchStrDB = @"https://www.google.ru/search?hl=ru&as_sitesearch=light.gg&as_q=";
        public Search()
        {
            InitializeComponent();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)Keys.Escape)
                {
                    e.Handled = true;
                    Close();
                    return;
                }
                if (e.KeyChar == (char)Keys.Enter)
                {
                    e.Handled = true;
                    try
                    {
                        System.Diagnostics.Process.Start(SearchStrDB + textBox1.Text);
                        Close();
                    }
                    catch { }
                }
            }
            catch { }
        }
    }
}
